<?php

namespace Xin\Setting\Exceptions;

class NotFountSettingItemException extends \Exception
{
    /**
     * @param string $key
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $key = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct("[$key] 配置不存在！", $code, $previous);
    }
}
