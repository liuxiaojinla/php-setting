<?php

namespace Xin\Setting\Laravel;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Xin\Setting\AbstractDatabaseRepository;
use Xin\Setting\Exceptions\NotFountSettingItemException;
use Xin\Support\Arr;

class ModelRepository extends AbstractDatabaseRepository
{
	use WithSearch;

	/**
	 * 构造一个新的查询器
	 * @return EloquentBuilder
	 */
	protected function newQuery()
	{
		if ($modelClass = $this->getConfig('model')) {
			return call_user_func([$modelClass, 'query']);
		}

		return DatabaseSetting::query();
	}

	/**
	 * 支持搜索的查询器
	 * @param array $search
	 * @return EloquentBuilder
	 */
	protected function newQueryWithSearch($search = [])
	{
		$query = $this->newQuery();
		if (method_exists($query, 'search')) {
			$query = $query->search($search);
		} else {
			$searchFields = $this->getSearchFields();
			$query = $this->withSearch($this->newQuery(), $searchFields, $search);
		}

		return $query;
	}

	/**
	 * @inerhitDoc
	 */
	protected function getExistsKeys(array $keys)
	{
		return $this->newQuery()->whereIn('name', $keys)->pluck('name')->toArray();
	}

	/**
	 * @inerhitDoc
	 */
	protected function create(array $data)
	{
		$builder = $this->newQuery();

		// 是否需要更新值
		if (isset($data['value'])) {
			$data['value'] = $this->serializer->serialize($data['type'], $data['value']);
		}

		return $builder->forceCreate($data)->toArray();
	}

	/**
	 * @inerhitDoc
	 * @throws NotFountSettingItemException
	 */
	protected function update($key, array $data)
	{
		// 构建查询器
		$builder = $this->newQuery();

		// 查询数据库数据
		$info = $builder->where('name', $key)->first();
		if (empty($info)) {
			throw new NotFountSettingItemException($key);
		}

		// 是否需要更新值
		if (isset($data['value'])) {
			$data['value'] = $this->serializer->serialize($info->type, $data['value']);
		}

		// 更新数据
		$info->forceFill($data)->save();

		return $info->toArray();
	}

	/**
	 * @inerhitDoc
	 */
	public function all(array $search = [])
	{
		$items = $this->newQueryWithSearch($search)->orderBy('sort')->get();

		return $this->optimizeItems($items);
	}

	/**
	 * @inerhitDoc
	 */
	public function paginate(array $search = [], array $paginate = [])
	{
		return $this->newQueryWithSearch($search)->select([
			"id", "title", "name", "alias", "type", "group", "remark", "sort",
			"status", "system", "display", "public", "updated_at", "created_at",
		])->orderByDesc('id')->paginate(
			Arr::get($paginate, 'perPage', 15),
			Arr::get($paginate, 'page', 1)
		)->toArray();
	}

	/**
	 * @inerhitDoc
	 */
	public function infos(array $keys)
	{
		$items = $this->newQuery()->whereIn('name', $keys)->get();

		return $this->optimizeItems($items);
	}

	/**
	 * 优化返回的结果集
	 * @param Collection $items
	 * @return array
	 */
	protected function optimizeItems(Collection $items)
	{
		return $items->reduce(function ($result, Model $item) {
			$item = $item->toArray();

			$value = $this->serializer->unserialize($item['type'], $item['value'], $isVarChange);
			if ($isVarChange) {
				$item['original_value'] = $item['value'];
				$item['value'] = $value;
			}

			$result[$item['name']] = $item;

			return $result;
		}, []);
	}

	/**
	 * @inerhitDoc
	 */
	public function deletes(array $keys)
	{
		$builder = $this->newQuery();

		// 查询要删除的数据是否存在
		return $builder->whereIn('name', $keys)->get()->map(function (Model $item) {
			$item->forceDelete();

			return $item->toArray();
		});
	}

	/**
	 * @inerhitDoc
	 */
	public function getMultiple($keys, $default = null)
	{
		$builder = $this->newQuery();
		$result = $builder->whereIn('name', $keys)->pluck('value', 'name')->toArray();

		foreach ($keys as $key) {
			if (!isset($result[$key])) {
				$result[$key] = $default;
			}
		}

		return $result;
	}

	/**
	 * @inerhitDoc
	 */
	public function setMultiple(array $settings)
	{
		$keys = array_keys($settings);

		return $this->newQuery()->whereIn('name', $keys)->get()->reduce(function ($result, Model $item) use ($settings) {
			$item = $item->toArray();
			$key = $item['name'];

			$value = $this->serializer->serialize($item['type'], $settings[$key]);

			$this->newQuery()->where('name', $key)->update([
				'value' => $value,
			]);

			$settings[$key] = $value;

			return $settings;
		}, []);
	}
}
