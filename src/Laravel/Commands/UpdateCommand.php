<?php

namespace Xin\Setting\Laravel\Commands;

use Illuminate\Console\Command;
use Xin\Setting\Contracts\Factory;

class UpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '刷新站点配置';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Factory $setting)
    {

        $setting->load(true);
        $setting->loadOnPublic(true);

        $this->output->success("已更新配置！");

        return 0;
    }

}
