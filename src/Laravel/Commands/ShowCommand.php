<?php

namespace Xin\Setting\Laravel\Commands;

use Illuminate\Console\Command;
use Xin\Setting\Contracts\Factory;

class ShowCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '查看站点配置';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Factory $setting)
    {
        $this->output->success(
            'default:' . json_encode($setting->load(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
        );
        $this->output->success(
            'public:' . json_encode($setting->loadOnPublic(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
        );
        $this->output->newLine();

        return 0;
    }

}
