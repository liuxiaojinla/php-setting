<?php

namespace Xin\Setting\Laravel\Commands;


use Illuminate\Console\Command;
use Xin\Setting\Contracts\Factory;

class ClearCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '清除站点配置';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Factory $setting)
    {
        $setting->clearCache();

        $this->output->success("已更新配置！");
        $this->output->newLine();

        return 0;
    }

}
