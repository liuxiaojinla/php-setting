<?php

namespace Xin\Setting\Facades;

use Psr\SimpleCache\InvalidArgumentException;
use think\Response as ThinkResponse;
use Xin\Setting\SettingManager;

if (class_exists(\Illuminate\Support\Facades\Facade::class)) {
    class Facade extends \Illuminate\Support\Facades\Facade
    {

    }
} elseif (class_exists(\think\Facade::class)) {
    class Facade extends \think\Facade
    {

    }
}

/**
 * @method static Repository repository(string $name)
 * @method static array load($refreshCache = false) throw InvalidArgumentException
 * @method static array loadOnPublic($refreshCache = false) throw InvalidArgumentException
 * @method static array loadToSystemConfig($refreshCache = false) throw InvalidArgumentException
 * @method static array clearCache($refreshCache = false) throw InvalidArgumentException
 * @see SettingManager
 */
class Setting extends Facade
{

    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     *
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'setting';
    }

    /**
     * 获取组件的注册名称。
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'setting';
    }

}
