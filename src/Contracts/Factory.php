<?php

namespace Xin\Setting\Contracts;

interface Factory
{
    /**
     * 获取一个配置仓库
     * @param string $name
     * @return Repository
     */
    public function repository($name): Repository;

    /**
     * 加载数据
     * @param bool $refreshCache
     * @return array
     */
    public function load($refreshCache = false);

    /**
     * 加载公开的数据
     * @param bool $refreshCache
     * @return array
     */
    public function loadOnPublic($refreshCache = false);

    /**
     * 加载数据到系统配置中
     * @param bool $refreshCache
     * @return array
     */
    public function loadToSystemConfig($refreshCache = false);

    /**
     * 清除缓存
     * @return void
     */
    public function clearCache();

}
