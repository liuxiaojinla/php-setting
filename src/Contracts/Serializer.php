<?php

namespace Xin\Setting\Contracts;

interface Serializer
{
    /**
     * 序列化数据
     * @param string $type
     * @param mixed $value
     * @return mixed
     */
    public function serialize($type, $value);

    /**
     * 反序列化数据
     * @param string $type
     * @param string $value
     * @return mixed
     */
    public function unserialize($type, $value, &$isChange = true);

}
