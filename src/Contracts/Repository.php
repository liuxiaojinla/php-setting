<?php

namespace Xin\Setting\Contracts;

use Xin\Setting\Exceptions\NotFountSettingItemException;

interface Repository
{
	/**
	 * 获取配置
	 * @param array $search
	 * @return array
	 */
	public function all(array $search = []);

	/**
	 * 分页获取配置
	 * @param array $search
	 * @param array $paginate
	 * @return array
	 */
	public function paginate(array $search = [], array $paginate = []);

	/**
	 * 获取配置信息
	 * @param string $key
	 * @return array
	 * @throws NotFountSettingItemException
	 */
	public function info($key, $fail = false);

	/**
	 * 获取一组配置信息
	 * @param array $keys
	 * @return array
	 */
	public function infos(array $keys);

	/**
	 * 添加或更新一个配置
	 * @param string $key
	 * @param array $info
	 * @return array
	 */
	public function upsert($key, $info);

	/**
	 * 添加或更新一组配置
	 * @param array $items
	 * @param bool $detaching
	 * @return array
	 */
	public function multipleUpsert(array $items, bool $detaching = false);

	/**
	 * 删除一个配置
	 * @param string $key
	 * @return array
	 */
	public function delete($key);

	/**
	 * 删除一组配置
	 * @param array $keys
	 * @return array
	 */
	public function deletes(array $keys);

	/**
	 * 获取配置值
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get($key, $default = null);

	/**
	 * 获取多个配置值
	 * @param array $keys
	 * @param mixed $default
	 * @return array
	 */
	public function getMultiple($keys, $default = null);

	/**
	 * 设置配置
	 * @param string $key
	 * @param mixed $value
	 * @return mixed
	 */
	public function set($key, $value);

	/**
	 * 设置一组配置
	 *
	 * @param array $settings
	 * @return array
	 */
	public function setMultiple(array $settings);

}
