<?php

namespace Xin\Setting\ThinkPHP\Commands;

use Psr\SimpleCache\InvalidArgumentException;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use Xin\Setting\SettingManager;

class ClearCommand extends Command
{

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('setting:clear')
            ->setDescription('清除站点配置');
    }

    /**
     * @param Input $input
     * @param Output $output
     * @throws InvalidArgumentException
     */
    protected function execute(Input $input, Output $output)
    {
        /** @var SettingManager $setting */
        $setting = app('setting');
        $setting->clearCache();

        $output->highlight("已更新配置！");
        $output->newLine();
    }

}
