<?php

namespace Xin\Setting\ThinkPHP\Commands;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use Xin\Setting\SettingManager;
use Xin\Thinkphp\Foundation\Setting\DatabaseSetting;

class UpdateCommand extends Command
{

	/**
	 * @inheritDoc
	 */
	protected function configure()
	{
		$this->setName('setting:update')
			->setDescription('刷新站点配置');
	}

	/**
	 * @param \think\console\Input $input
	 * @param \think\console\Output $output
	 */
	protected function execute(Input $input, Output $output)
	{
        /** @var SettingManager $setting */
        $setting = app('setting');

        $setting->load(true);
        $setting->loadOnPublic(true);

		$output->highlight("已更新配置！");
	}

}
