<?php

namespace Xin\Setting\ThinkPHP\Commands;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use Xin\Setting\SettingManager;
use Xin\Thinkphp\Foundation\Setting\DatabaseSetting;

class ShowCommand extends Command
{

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('setting:show')
            ->setDescription('查看站点配置');
    }

    /**
     * @param \think\console\Input $input
     * @param \think\console\Output $output
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function execute(Input $input, Output $output)
    {
        /** @var SettingManager $setting */
        $setting = app('setting');

        $output->highlight(
            'default:' . json_encode($setting->load(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
        );
        $output->highlight(
            'public:' . json_encode($setting->loadOnPublic(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
        );

        $output->newLine();
    }

}
