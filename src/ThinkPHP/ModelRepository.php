<?php

namespace Xin\Setting\ThinkPHP;

use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Model;
use Xin\Setting\AbstractDatabaseRepository;
use Xin\Setting\Exceptions\NotFountSettingItemException;

class ModelRepository extends AbstractDatabaseRepository
{
	/**
	 * 构造一个新的查询器
	 * @return Model
	 */
	protected function newQuery()
	{
		if ($modelClass = $this->getConfig('model')) {
			return new $modelClass;
		}

		return new DatabaseSetting;
	}

	/**
	 * 支持搜索的查询器
	 * @param array $search
	 * @return Model
	 */
	protected function newQueryWithSearch(array $search = [])
	{
		$query = $this->newQuery();
		if (method_exists($query, 'search')) {
			return $query->search($search);
		}

		$searchFields = $this->getSearchFields();

		return $this->newQuery()->withSearch($searchFields, $search);
	}

	/**
	 * @inerhitDoc
	 */
	protected function getExistsKeys(array $keys)
	{
		return $this->newQuery()->whereIn('name', $keys)->column('name');
	}

	/**
	 * @inerhitDoc
	 */
	protected function create(array $data)
	{
		$builder = $this->newQuery();

		$builder->data($data)->save();

		return $builder->toArray();
	}

	/**
	 * @inerhitDoc
	 * @throws NotFountSettingItemException
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 */
	protected function update($key, array $data)
	{
		// 构建查询器
		$builder = $this->newQuery();

		// 查询数据库数据
		/** @var DatabaseSetting $info */
		$info = $builder->where('name', $key)->find();
		if (empty($info)) {
			throw new NotFountSettingItemException($key);
		}

		// 是否需要更新值
		if (isset($data['value'])) {
			$data['value'] = $this->serializer->serialize($info->type, $data['value']);
		}

		$info->data($data)->save();

		return $info->toArray();
	}

	/**
	 * @inerhitDoc
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 */
	public function all(array $search = [])
	{
		$items = $this->newQueryWithSearch($search)->order('sort')->select();

		return $this->optimizeItems($items);
	}

	/**
	 * @inerhitDoc
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 */
	public function paginate(array $search = [], array $paginate = [])
	{
		return $this->newQueryWithSearch($search)->field([
			"id", "title", "name", "alias", "type", "group", "remark", "sort",
			"status", "system", "display", "public", "updated_at", "created_at",
		])->order('id', 'desc')->paginate($paginate)->toArray();
	}

	/**
	 * @inerhitDoc
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 */
	public function infos(array $keys)
	{
		$items = $this->newQuery()->whereIn('name', $keys)->select();

		return $this->optimizeItems($items);
	}

	/**
	 * 优化返回的结果集
	 * @param Collection $items
	 * @return array
	 */
	protected function optimizeItems(Collection $items)
	{
		return $items->reduce(function ($result, Model $item) {
			$item = $item->toArray();

			$value = $this->serializer->unserialize($item['type'], $item['value'], $isVarChange);
			if ($isVarChange) {
				$item['original_value'] = $item['value'];
				$item['value'] = $value;
			}

			$result[$item['name']] = $item;

			return $result;
		}, []);
	}

	/**
	 * @inerhitDoc
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 */
	public function deletes(array $keys)
	{
		$builder = $this->newQuery();

		// 查询要删除的数据是否存在
		return $builder->whereIn('name', $keys)->select()->map(function (Model $item) {
			if (method_exists($item, 'force')) {
				$item->force()->delete();
			} else {
				$item->delete();
			}

			return $item->toArray();
		});
	}

	/**
	 * @inerhitDoc
	 */
	public function getMultiple($keys, $default = null)
	{
		$builder = $this->newQuery();
		$result = $builder->whereIn('name', $keys)->column('value', 'name');

		foreach ($keys as $key) {
			if (!isset($result[$key])) {
				$result[$key] = $default;
			}
		}

		return $result;
	}

	/**
	 * @inerhitDoc
	 * @throws DataNotFoundException
	 * @throws DbException
	 * @throws ModelNotFoundException
	 */
	public function setMultiple(array $settings)
	{
		$keys = array_keys($settings);

		return $this->newQuery()->whereIn('name', $keys)->select()->reduce(function ($result, Model $item) use ($settings) {
			$item = $item->toArray();

			$key = $item['name'];

			$value = $this->serializer->serialize($item['type'], $settings[$key]);

			$this->newQuery()->where('name', $key)->update([
				'value' => $value,
			]);

			$settings[$key] = $value;

			return $settings;
		}, []);
	}
}
