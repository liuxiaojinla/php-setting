# Setting | 快速集成应用程序配置管理

## 介绍

快速集成应用程序配置管理

## 安装教程

`composer require xin/setting`

## 使用

### 使用注解的方式

```php
<?php
/** @var \Xin\Setting\SettingManager $setting */
$setting = app('setting');

// 加载配置
$data = $setting->load();
var_dump($data);

// 加载配置到系统配置中
$setting->loadToSystemConfig();

// 添加配置
$setting->upsert('hello','world');

// 获取配置
$value = $setting->info('hello');
var_dump($value);

```

### 使用门面的方式

```php
<?php
use Xin\Setting\Facades\Setting;
// 加载配置
$data = Setting::load();
var_dump($data);

// 加载配置到系统配置中
Setting::loadToSystemConfig();

// 添加配置
Setting::upsert('hello','world');

// 获取配置
$value = Setting::info('hello');
var_dump($value);

```
